package com.example.proyectoestructuras;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class BaseDeDatos extends SQLiteOpenHelper {

    public BaseDeDatos(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //Fecha de inicio: 04/11/2020
        //Fecha de ultima modificacion: 09/11/2020
        //método inicia y crea las tablas donde se va a hacer la copia de seguridad de cada dato del usuario.
        sqLiteDatabase.execSQL("create table usuarios(cedula int primary key, nombre text, direccion text, telefono int)");
        sqLiteDatabase.execSQL("create table amigos(cedula int, fecha text, cedulaAmigo int, tag text)");
        sqLiteDatabase.execSQL("create table tag(nombre text, descripcion text)");
        sqLiteDatabase.execSQL("create table intereses(cedula int, id int, tag text)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
