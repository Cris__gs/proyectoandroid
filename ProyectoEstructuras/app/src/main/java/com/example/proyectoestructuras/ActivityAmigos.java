package com.example.proyectoestructuras;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityAmigos extends AppCompatActivity {
    Metodos met = new Metodos();
    Spinner spinner1;
    TextView txtv1, txtv2, txtv3, txtv4, txtv5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Fecha de inicio: 07/11/2020
        //Fecha de ultima modificacion: 07/11/2020
        //método que inicia el frame junto con sus widgets, ademas de que recorre la lista de amigos del usuario y muestra sus numeros de cedula en el spinner.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amigos);
        this.setTitle(R.string.TituloAmigos);
        spinner1 = (Spinner)findViewById(R.id.spinner_amigos);

        txtv1 = (TextView)findViewById(R.id.txtv_nombre);
        txtv2 = (TextView)findViewById(R.id.txtv_cedula);
        txtv3 = (TextView)findViewById(R.id.txtv_telefono);
        txtv4 = (TextView)findViewById(R.id.txtv_ubicacion);
        txtv5 = (TextView)findViewById(R.id.txtv_fecha);

        SharedPreferences preferencias = getSharedPreferences("agenda", Context.MODE_PRIVATE);
        String datos = preferencias.getString("cedula", "");

        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase dato = admin.getWritableDatabase();

        Cursor filaUsuario = dato.rawQuery("select * from usuarios", null);
        int validar = 1;
        filaUsuario.moveToFirst();
        while (validar <= filaUsuario.getCount()) {
            int cedula = filaUsuario.getInt(0);
            String nombre = filaUsuario.getString(1);
            String direccion = filaUsuario.getString(2);
            int telefono = filaUsuario.getInt(3);
            met.insertarOrdenadoUsuario(cedula, nombre, direccion, telefono);
            validar++;
            filaUsuario.moveToNext();
        }

        Cursor filaTag = dato.rawQuery("select * from tag", null);
        validar = 1;
        filaTag.moveToFirst();
        while (validar <= filaTag.getCount()) {
            String nombre = filaTag.getString(0);
            String descripcion = filaTag.getString(1);
            met.insertarTag(nombre, descripcion);
            validar++;
            filaTag.moveToNext();
        }

        Cursor filaInteres = dato.rawQuery("select * from intereses", null);
        validar = 1;
        filaInteres.moveToFirst();
        while (validar <= filaInteres.getCount()) {
            int cedula = filaInteres.getInt(0);
            int id = filaInteres.getInt(1);
            String tag = filaInteres.getString(2);
            met.insertarInteresUsuario(cedula, tag);
            validar++;
            filaInteres.moveToNext();
        }

        Cursor filaAmigos = dato.rawQuery("select * from amigos", null);
        validar = 1;
        filaAmigos.moveToFirst();
        while (validar <= filaAmigos.getCount()) {
            int cedula = filaAmigos.getInt(0);
            String fecha = filaAmigos.getString(1);
            int cedulaAmigo = filaAmigos.getInt(2);
            String tag = filaAmigos.getString(3);
            met.insertarAmigo(cedula, fecha, cedulaAmigo, tag);
            validar++;
            filaAmigos.moveToNext();
        }

        dato.close();

        Usuario usuario = met.buscarCedula(Integer.parseInt(datos));
        if(usuario.sigAmigo != null){
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, met.mostrarAmigos(Integer.parseInt(datos)));
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner1.setAdapter(adapter);
        }
        else{
            Intent siguiente = new Intent(this, ActivityInsertarAmigo.class);
            startActivity(siguiente);
        }

    }
    public void volverMenu(View view){
        //Fecha de inicio: 06/11/2020
        //Fecha de ultima modificacion: 06/11/2020
        //método que devuelve al usuario al menu principal
        Intent siguiente = new Intent(this, ActivitySpinner.class);
        startActivity(siguiente);
    }

    public void IrRegistrarAmigo(View view){
        //Fecha de inicio: 06/11/2020
        //Fecha de ultima modificacion: 06/11/2020
        //método que devuelve al usuario al menu principal
        Intent siguiente = new Intent(this, ActivityInsertarAmigo.class);
        startActivity(siguiente);
    }

    public void mostrar(View view){
        //Fecha de inicio: 06/11/2020
        //Fecha de ultima modificacion: 07/11/2020
        //método que busca en la lista a los amigos por medio de la cedula que haya seleccionado en el spinner, de este modo busca
        SharedPreferences preferencias = getSharedPreferences("agenda", Context.MODE_PRIVATE);
        String datos = preferencias.getString("cedula", "");

        Usuario usuario = met.buscarCedula(Integer.parseInt(spinner1.getSelectedItem().toString()));

        txtv1.setText(usuario.nombre);
        txtv2.setText(String.valueOf(usuario.cedula));
        txtv3.setText(String.valueOf(usuario.telefono));
        txtv4.setText(usuario.direccion);

        Usuario usuario1 = met.buscarCedula(Integer.parseInt(datos));

        String fecha = null;


        for (int i = 0; i < usuario1.sigAmigo.sigUsuario.size(); i++){
            if (usuario1.sigAmigo.sigUsuario.get(i).cedula == Integer.parseInt(spinner1.getSelectedItem().toString())){
                fecha = String.valueOf(usuario1.sigAmigo.fecha);

            }
        }

        txtv5.setText(fecha);
    }


    public void eliminar(View view){
        //Fecha de inicio: 06/11/2020
        //Fecha de ultima modificacion: 07/11/2020
        //método que busca en la lista a los amigos por medio de la cedula que haya seleccionado en el spinner, de este modo busca
        SharedPreferences preferencias = getSharedPreferences("agenda", Context.MODE_PRIVATE);
        String datos = preferencias.getString("cedula", "");

        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase dato = admin.getWritableDatabase();

        int cedulaA = Integer.parseInt(spinner1.getSelectedItem().toString());
        int cedula = Integer.parseInt(datos);

        met.eliminarAmigo(cedula,cedulaA);
        int cantidad = dato.delete("amigos", "cedulaAmigo = ? AND cedula = ?", new String[] {String.valueOf(cedulaA), String.valueOf(cedula)});
        dato.close();
        if(cantidad == 1){
            Usuario usuario = met.buscarCedula(Integer.parseInt(datos));
            if(usuario.sigAmigo != null){
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, met.mostrarAmigos(Integer.parseInt(datos)));
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(adapter);
            }
            else{
                Intent siguiente = new Intent(this, ActivityInsertarAmigo.class);
                startActivity(siguiente);
            }
            Toast.makeText(this, "Amigo eliminado correctamente", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Amigo no encontrado", Toast.LENGTH_SHORT).show();
        }

    }


}
