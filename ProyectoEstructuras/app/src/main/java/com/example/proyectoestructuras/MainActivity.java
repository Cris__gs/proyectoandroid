package com.example.proyectoestructuras;

        import androidx.appcompat.app.AppCompatActivity;

        import android.content.ContentValues;
        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.database.Cursor;
        import android.database.sqlite.SQLiteDatabase;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.EditText;
        import android.widget.TextView;
        import android.widget.Toast;



public class MainActivity extends AppCompatActivity {
    Metodos met = new Metodos();
    private TextView coordenadas;
    private EditText nombre, cedula, telefono;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Fecha de inicio: 04/11/2020
        //Fecha de última modificación: 09/11/2020
        //Este metodo inicializa los componentes de la interfaz en los cuales se mostrarán los datos del usuario registrado.
        //Además, recibe desde el activity ubicación, las coordenadas que se le asignarán al atributo ubicación del usuario.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setTitle(R.string.title_activity_Registro);
        nombre = (EditText)findViewById(R.id.txt_nombre2);
        cedula = (EditText)findViewById(R.id.txt_cedula2);
        telefono = (EditText)findViewById(R.id.txt_telefono2);
        coordenadas = (TextView) findViewById(R.id.textView3);
        String dato = getIntent().getStringExtra("dato");
        coordenadas.setText(dato);

    }

    public void ubicacion (View view){
        //Fecha de inicio: 04/11/2020
        //Fecha de última modificación: 09/11/2020
        //Este metodo muestra la activity en la cual nuestro usuario registra su ubicación.
        Intent siguiente = new Intent(this, Ubicacion.class);
        startActivity(siguiente);

    }

    public void registrar (View view){
        //Fecha de inicio: 04/11/2020
        //Fecha de última modificación: 09/11/2020
        //Este método cumple la función de registrar los datos que fueron ingresados por el usuario, a las diferentes listas y también a la base de datos.
        //En caso de que el usuario ya esté registrado, este lo busca por medio de su cédula en la base de datos, y en caso de encontrarlo, se le mostrará...
        //un mensaje de que ya se encuentra regsitrado.
        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase datos = admin.getWritableDatabase();

        String nombre_string = nombre.getText().toString();
        String cedula_string = cedula.getText().toString();
        String coordenadas_String = coordenadas.getText().toString();
        String telefono_string = telefono.getText().toString();

        int cedula_int = Integer.valueOf(cedula_string);
        int telefono_int = Integer.valueOf(telefono_string);

        if (nombre_string.isEmpty()){
            Toast.makeText(this, "Debe ingresar su nombre", Toast.LENGTH_SHORT).show();
        }else if (cedula_string.isEmpty()){
            Toast.makeText(this, "Debe ingresar su numero de cedula", Toast.LENGTH_SHORT).show();
        }else if (telefono_string.isEmpty()) {
            Toast.makeText(this, "Debe ingresar su numero de telefono", Toast.LENGTH_SHORT).show();
        }else if (coordenadas_String.isEmpty()){
            Toast.makeText(this, "Debe seleccionar su ubicacion", Toast.LENGTH_SHORT).show();
        }else{
            Cursor fila = datos.rawQuery("select * from usuarios", null);


            if(fila.getCount() == 0){
                if(met.insertarOrdenadoUsuario(cedula_int, nombre_string, coordenadas_String, telefono_int)){
                    ContentValues registro = new ContentValues();
                    registro.put("cedula", cedula_int);
                    registro.put("nombre", nombre_string);
                    registro.put("direccion", coordenadas_String);
                    registro.put("telefono", telefono_string);
                    datos.insert("usuarios", null, registro);
                }
                Toast.makeText(this, "Registrado", Toast.LENGTH_SHORT).show();
                Intent siguiente = new Intent(this, Intereses.class);
                siguiente.putExtra("cedulaCopia", cedula_string);
                startActivity(siguiente);
            }else{
                int validar = 1;
                fila.moveToFirst();
                while(validar <= fila.getCount()){
                    int cedula = fila.getInt(0);
                    String nombre = fila.getString(1);
                    String direccion = fila.getString(2);
                    int telefono = fila.getInt(3);
                    met.insertarOrdenadoUsuario(cedula, nombre, direccion, telefono);
                    validar++;
                    fila.moveToNext();
                }
                datos.delete("usuarios", null, null);
                if(met.insertarOrdenadoUsuario(cedula_int, nombre_string, coordenadas_String, telefono_int)){
                    Usuario aux = met.inicioUsuario;
                    ContentValues registro = new ContentValues();
                    if (aux != null) {
                        registro.put("cedula", aux.cedula);
                        registro.put("nombre", aux.nombre);
                        registro.put("direccion", aux.direccion);
                        registro.put("telefono", aux.telefono);
                        datos.insert("usuarios", null, registro);
                        aux = aux.sig;
                    }
                    while (aux != met.inicioUsuario) {
                        registro.put("cedula", aux.cedula);
                        registro.put("nombre", aux.nombre);
                        registro.put("direccion", aux.direccion);
                        registro.put("telefono", aux.telefono);
                        datos.insert("usuarios", null, registro);
                        aux = aux.sig;
                    }

                    Toast.makeText(this, "Registrado", Toast.LENGTH_SHORT).show();

                    Intent siguiente = new Intent(this, Intereses.class);
                    siguiente.putExtra("cedulaCopia", cedula_string);
                    startActivity(siguiente);

                }else{
                    Toast.makeText(this, "Cédula repetida", Toast.LENGTH_SHORT).show();
                }
            }
            datos.close();
        }



    }
    /*
    public void imprimir(View view){
        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase datos = admin.getWritableDatabase();
        Cursor fila = datos.rawQuery("select * from usuarios", null);
        int validar = 1;
        fila.moveToFirst();
        while(validar <= fila.getCount()){
            int cedula = fila.getInt(0);
            String nombre = fila.getString(1);
            String direccion = fila.getString(2);
            int telefono = fila.getInt(3);
            met.insertarOrdenadoUsuario(cedula, nombre, direccion, telefono);
            validar++;
            fila.moveToNext();
        }
        Usuario aux = met.inicioUsuario;
        if (aux != null) {
            String cedula_string = String.valueOf(aux.cedula);
            String telefono_string = String.valueOf(aux.telefono);
            Log.i("Nombre", aux.nombre);
            Log.i("Cédula", cedula_string);
            Log.i("Dirección", aux.direccion);
            Log.i("Teléfono", telefono_string);
            aux = aux.sig;
        }
        while (aux != met.inicioUsuario) {
            String cedula_string = String.valueOf(aux.cedula);
            String telefono_string = String.valueOf(aux.telefono);
            Log.i("Nombre", aux.nombre);
            Log.i("Cédula", cedula_string);
            Log.i("Dirección", aux.direccion);
            Log.i("Teléfono", telefono_string);
            aux = aux.sig;
        }
    }
     */

}