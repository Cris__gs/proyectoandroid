package com.example.proyectoestructuras;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class login extends AppCompatActivity {

    Metodos met = new Metodos();
    private EditText etNombre, etCedula;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Fecha de inicio: 5/11/2020
        //Fecha de última modificación: 8/11/2020
        //Este método inicia el frame junto con sus diferentes widgets, además si la base de datos se encuentra vacía, construye las listas con 30... (Linea de abajo ->)
        //personas para temas de pruebas, y así contar con una mayor variedad de amigos a agregar. Además de insertar los tags a la lista de tags... (Linea de abajo ->)
        //esta información se carga a la base de datos.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.setTitle(R.string.Login);

        etNombre = (EditText)findViewById(R.id.txt_nombre1);
        etCedula = (EditText)findViewById(R.id.txt_cedula1);

        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase datos = admin.getWritableDatabase();

        Cursor fila = datos.rawQuery("select * from usuarios", null);

        if(fila.getCount() == 0) {

            met.insertarOrdenadoUsuario(1046901, "María Carmen", "(-14.15, -51.44)", 80173475);
            met.insertarOrdenadoUsuario(4278400, "María", "(38.87, -97.45)", 83879749);
            met.insertarOrdenadoUsuario(5806827, "Carmen", "(-34.58, -64.15)", 86920548);
            met.insertarOrdenadoUsuario(5785026, "Josefa", "(55.07, -103.59)", 86404495);
            met.insertarOrdenadoUsuario(6618956, "Ana María", "(52.35, -1.71)", 80851232);

            met.insertarOrdenadoUsuario(6500621, "Isabel", "(22.61, -102.37)", 87352527);
            met.insertarOrdenadoUsuario(1715108, "María Pilar", "(28.91, 0.58)", 88055747);
            met.insertarOrdenadoUsuario(2686326, "María Dolores", "(19.76, -76.16)", 81319818);
            met.insertarOrdenadoUsuario(4473142, "Laura", "(-11.02, -74.86)", 83614202);
            met.insertarOrdenadoUsuario(7544108, "María Teresa", "(-19.58, -64.42)", 87311194);

            met.insertarOrdenadoUsuario(5093841, "Antonio", "(-7.37, -67.26)", 84049151);
            met.insertarOrdenadoUsuario(2466327, "José", "(-30.34, 25.85)", 80649365);
            met.insertarOrdenadoUsuario(1219055, "Manuel ", "(9.59, -83.90)", 84434412);
            met.insertarOrdenadoUsuario(8064942, "Francisco", "(60.32, 8.65)", 86697468);
            met.insertarOrdenadoUsuario(1087088, "David", "(53.52, -73.47)", 84871272);

            met.insertarOrdenadoUsuario(3506661, "Juan", "(3.52, -70.06)", 88719690);
            met.insertarOrdenadoUsuario(4099195, "José Antonio", "(40.63, -112.62)", 88112577);
            met.insertarOrdenadoUsuario(3507831, "Javier", "(-31.58, -53.14)", 87011387);
            met.insertarOrdenadoUsuario(7413038, "José Luis", "(68.31, -155.51)", 83536664);
            met.insertarOrdenadoUsuario(8760028, "Daniel", "(76.84, -44.61)", 89474455);

            met.insertarOrdenadoUsuario(1062732, "Lucía", "(3.25, -56.63)", 85686646);
            met.insertarOrdenadoUsuario(6746700, "Martina", "(64.40, -19.09)", 85345604);
            met.insertarOrdenadoUsuario(8727732, "Sofía", "(28.22, -81.32)", 86858920);
            met.insertarOrdenadoUsuario(3198757, "Paula", "(19.43, -155.29)", 86746453);
            met.insertarOrdenadoUsuario(2294382, "Daniela", "(18.55, -70.31)", 82221528);

            met.insertarOrdenadoUsuario(2716076, "Hugo", "(40.43, -77.23)", 87452555);
            met.insertarOrdenadoUsuario(8320919, "Pablo", "(43.76, -108.87)", 87200959);
            met.insertarOrdenadoUsuario(3802386, "Alejandro", "(42.07, 13.04)", 80917436);
            met.insertarOrdenadoUsuario(7363034, "Lucas", "(61.56, 26.46)", 84974938);
            met.insertarOrdenadoUsuario(5855228, "Álvaro", "(4.01, 102.08)", 88701617);

            String[] intereses = {"Gaming", "Lectura", "Peliculas", "Series", "Programacion", "Ciencias", "Deportes", "Gastronomia", "Musica", "Matematicas", "Historia", "Vehiculos", "Fotografia", "Pintura", "Noticias",
                    "Gaming", "Lectura", "Peliculas", "Series", "Programacion", "Ciencias", "Deportes", "Gastronomia", "Musica", "Matematicas", "Historia", "Vehiculos", "Fotografia", "Pintura", "Noticias"};

            met.insertarTag("Gaming", "Comunidad de amantes de los video juegos(La mejor)");
            met.insertarTag("Lectura", "Comunidad de amantes de los libros");
            met.insertarTag("Peliculas", "Comunidad de amantes de las peliculas");
            met.insertarTag("Series", "Comunidad de amantes de los Series");
            met.insertarTag("Programacion", "Comunidad de amantes del codigo");
            met.insertarTag("Ciencias", "Comunidad de amantes de la ciencia");
            met.insertarTag("Deportes", "Comunidad de amantes del deporte");
            met.insertarTag("Gastronomia", "Comunidad de amantes de la cocina");
            met.insertarTag("Musica", "Comunidad de amantes de la musica");
            met.insertarTag("Matematicas", "Comunidad de los locos amantes de las matematicas");
            met.insertarTag("Historia", "Comunidad de amantes de la historia");
            met.insertarTag("Vehiculos", "Comunidad de amantes de los vehiculos");
            met.insertarTag("Fotografia", "Comunidad de amantes de la fotografia");
            met.insertarTag("Pintura", "Comunidad de amantes de las pinturas");
            met.insertarTag("Noticias", "Comunidad de interesados por las noticias");


            Usuario aux = met.inicioUsuario;
            if (aux != null) {
                met.insertarInteresUsuario(aux.cedula, intereses[0]);
                aux = aux.sig;
            }
            for (int i = 1; i < intereses.length; i++) {
                if (aux != met.inicioUsuario) {
                    met.insertarInteresUsuario(aux.cedula, intereses[i]);
                    aux = aux.sig;
                }
            }

            datos.delete("tag", null, null);
            Tag aux4 = met.inicioTag;
            ContentValues registro = new ContentValues();
            if (aux4 != null) {
                registro.put("nombre", aux4.Nombre);
                registro.put("descripcion", aux4.Descripcion);
                datos.insert("tag", null, registro);
                aux4 = aux4.sig;
            }
            while (aux4 != met.inicioTag) {
                registro.put("nombre", aux4.Nombre);
                registro.put("descripcion", aux4.Descripcion);
                datos.insert("tag", null, registro);
                aux4 = aux4.sig;
            }

            Usuario aux2 = met.inicioUsuario;
            ContentValues registro2 = new ContentValues();
            if (aux2 != null) {
                registro2.put("cedula", aux2.cedula);
                registro2.put("nombre", aux2.nombre);
                registro2.put("direccion", aux2.direccion);
                registro2.put("telefono", aux2.telefono);
                datos.insert("usuarios", null, registro2);
                SublistaIntereses aux3 = aux2.sigInteres;
                ContentValues registro3 = new ContentValues();
                while (aux3 != null) {
                    registro3.put("cedula", aux2.cedula);
                    registro3.put("id", aux3.id);
                    registro3.put("tag", aux3.sigTag.Nombre);
                    datos.insert("intereses", null, registro3);
                    aux3 = aux3.sig;
                }
                aux2 = aux2.sig;
            }
            while (aux2 != met.inicioUsuario) {
                registro2.put("cedula", aux2.cedula);
                registro2.put("nombre", aux2.nombre);
                registro2.put("direccion", aux2.direccion);
                registro2.put("telefono", aux2.telefono);
                datos.insert("usuarios", null, registro2);
                SublistaIntereses aux3 = aux2.sigInteres;
                ContentValues registro3 = new ContentValues();
                while (aux3 != null) {
                    registro3.put("cedula", aux2.cedula);
                    registro3.put("id", aux3.id);
                    registro3.put("tag", aux3.sigTag.Nombre);
                    datos.insert("intereses", null, registro3);
                    aux3 = aux3.sig;
                }
                aux2 = aux2.sig;
            }
        }

        datos.close();
    }

    @Override
    protected void onDestroy() {
        //Fecha de inicio: 5/11/2020
        //Fecha de última modificación: 8/11/2020
        //Este metodo cumple la función de brindarle un mensaje al usuario, al destruirse la ventana
        super.onDestroy();
        Toast.makeText(this, "OnDestroy", Toast.LENGTH_SHORT).show();
        // La actividad está a punto de ser destruida.
    }

    public void siguiente (View view){
        //Fecha de inicio: 5/11/2020
        //Fecha de última modificación: 8/11/2020
        //Metodo que le permite al usuario, desplazarse a otra activity.
        Intent siguiente = new Intent(this, MainActivity.class);
        startActivity(siguiente);

    }

    public  void login (View view){
        //Fecha de inicio: 5/11/2020
        //Fecha de última modificación: 8/11/2020
        //Metodo que le permite a un usuario ya registrado, ingresar al sistema por medio de su nombre y número de cédula.
        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase datos = admin.getWritableDatabase();

        String nombre_string = etNombre.getText().toString();
        String cedula_string = etCedula.getText().toString();

        if (nombre_string.isEmpty()){
            Toast.makeText(this, "Debe ingresar su nombre", Toast.LENGTH_SHORT).show();
        }if(cedula_string.isEmpty()){
            Toast.makeText(this, "Debe ingresar su numero de cedula", Toast.LENGTH_SHORT).show();
        }else{
            int cedula_int = Integer.valueOf(cedula_string);
            Cursor fila = datos.rawQuery("select nombre, direccion, telefono from usuarios where cedula=" + cedula_int, null);
            if(fila.moveToFirst()){
                Toast.makeText(this, "Logueando...", Toast.LENGTH_SHORT).show();
                SharedPreferences preferencias = getSharedPreferences("agenda", Context.MODE_PRIVATE);
                SharedPreferences.Editor obj_editor = preferencias.edit();
                obj_editor.putString("cedula", cedula_string);
                obj_editor.commit();
                Intent siguiente = new Intent(this, ActivitySpinner.class);
                startActivity(siguiente);
            }
            else{
                Toast.makeText(this, "El usuario no existe, por favor registrese", Toast.LENGTH_SHORT).show();
            }
        }
        datos.close();
    }
}