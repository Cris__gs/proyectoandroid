package com.example.proyectoestructuras;

public class Usuario {
    //Fecha de inicio: 04/11/2020
    //Fecha de última modificación:05/11/2020
    //Es una lista doble circular que tiene un enlace a la sublista Intereses y a la lista Amigo, además posee 4 atributos, "cedula","nombre","direccion","telefono".
    int cedula;
    String nombre;
    String direccion;//Por el momento sin utilizar Google Maps con android
    int telefono;
    Usuario sig;
    Usuario ant;
    Amigo sigAmigo;
    SublistaIntereses sigInteres;

    public Usuario(int cedula, String nombre, String direccion, int telefono) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
    }
}
