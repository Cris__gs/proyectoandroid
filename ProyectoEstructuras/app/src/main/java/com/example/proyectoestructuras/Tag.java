package com.example.proyectoestructuras;

public class Tag {
    //Fecha de inicio: 04/11/2020
    //Fecha de última modificación:05/11/2020
    //Es una lista simple circular que posee dos atributos, "Nombre" y "Descripción"
    String Nombre;
    String Descripcion;
    Tag sig;

    public Tag(String Nombre, String Descripcion) {
        this.Nombre = Nombre;
        this.Descripcion = Descripcion;
    }
}
