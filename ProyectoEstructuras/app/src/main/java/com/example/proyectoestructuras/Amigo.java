package com.example.proyectoestructuras;

import java.util.ArrayList;


public class Amigo {
    //Fecha de inicio: 04/11/2020
    //Fecha de última modificación:05/11/2020
    //Es una lista doble , que posee una enlace a la sublista Intereses, también posee una Array de punteros, y posee un atributo de "fecha".
    String fecha;
    Amigo sig, ant;
    SublistaIntereses sigInteres;
    ArrayList<Usuario> sigUsuario = new ArrayList<Usuario>();

    public Amigo(String fecha) {
        this.fecha = fecha;
    }
}