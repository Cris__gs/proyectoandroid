package com.example.proyectoestructuras;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ActivityInsertarAmigo extends AppCompatActivity {

    Spinner spinner1;
    Spinner spinner2;
    TextView txtV1;
    EditText etxt_multi;
    EditText etF;
    Metodos met = new Metodos();
    private int dia, mes, ano;

    String cedulaUsuario = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Fecha de inicio: 08/11/2020
        //Fecha de última modificación:09/11/2020
        //Este metodo extrae los intereses que el usuario tiene agregados en su cuenta, y los carga en un spinner con el cual va a poder filtrar la busqueda de amigos segun sus tags.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insertar_amigo);

        spinner1 = (Spinner)findViewById(R.id.spinnerFriends);
        spinner2 = (Spinner)findViewById(R.id.spinnerIntereses);
        txtV1 = (TextView)findViewById(R.id.txtV_nombre);
        etxt_multi = (EditText)findViewById(R.id.etxt_multi);
        etF = (EditText)findViewById(R.id.fechaAmiguis);

        SharedPreferences preferencias = getSharedPreferences("agenda", Context.MODE_PRIVATE);
        String datos = preferencias.getString("cedula", "");

        cedulaUsuario = datos;

        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase dato = admin.getWritableDatabase();

        ArrayList<String> tagsUsuario = new ArrayList();
        Cursor filaInteres = dato.rawQuery("select tag from intereses where cedula=" + Integer.valueOf(datos), null);
        int validar = 1;
        filaInteres.moveToFirst();
        while (validar <= filaInteres.getCount()) {
            String nombre = filaInteres.getString(0);
            tagsUsuario.add(nombre);
            validar++;
            filaInteres.moveToNext();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, tagsUsuario);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner2.setAdapter(adapter);

        dato.close();

    }

    public void posiblesAmigos(View view){
        //Fecha de inicio: 08/11/2020
        //Fecha de última modificación:09/11/2020
        //Este metodo extrae los usuarios que tengan dentro de susu intereses el interes qued el usuario tiene seleccionado en su spinner de intereses todos estos usuarios, y muestra sus numeros de cedula
        // en un nuevo spinner.
        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase dato = admin.getWritableDatabase();

        String seleccion = spinner2.getSelectedItem().toString();

        ArrayList<String> posiblesAmigos = new ArrayList();
        Cursor filaTag = dato.rawQuery("select * from intereses", null);

        int validar = 1;
        filaTag.moveToFirst();
        while (validar <= filaTag.getCount()) {
            int cedula = filaTag.getInt(0);
            int id = filaTag.getInt(1);
            String nombre = filaTag.getString(2);
            if(nombre.equals(seleccion) && cedula != Integer.valueOf(cedulaUsuario)){
                Cursor filaAmigo = dato.rawQuery("select * from amigos where cedulaAmigo = ? AND cedula = ?", new String[] {String.valueOf(cedula), String.valueOf(cedulaUsuario)});
                if(filaAmigo.getCount() == 0){
                    posiblesAmigos.add(String.valueOf(cedula));
                }
            }
            validar++;
            filaTag.moveToNext();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, posiblesAmigos);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner1.setAdapter(adapter);

        dato.close();
    }

    public void Fecha(View view){
        //Fecha de inicio: 08/11/2020
        //Fecha de última modificación:09/11/2020
        //Este metodo extrae la fecha que el usuario desee seleccionar con respecto a el dia que va aagregar a un amigo.
        final Calendar c = Calendar.getInstance();
        dia = c.get(Calendar.DAY_OF_MONTH);
        mes = c.get(Calendar.MONTH);
        ano = c.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                etF.setText(dayOfMonth + "/" + (monthOfYear+1) + "/" + year);
            }
        }
                , dia, mes, ano);
        datePickerDialog.show();
    }

    public void mostrar(View view){
        //Fecha de inicio: 08/11/2020
        //Fecha de última modificación:09/11/2020
        //Este metodo extrae los datos del usuario seleccionado en el segundo spinnes, estos son mostrados en diferentes text views.
        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase dato = admin.getWritableDatabase();

        txtV1.setText("");

        String seleccion = spinner1.getSelectedItem().toString();

        Cursor fila = dato.rawQuery("select nombre from usuarios where cedula=" + Integer.valueOf(seleccion), null);

        fila.moveToFirst();
        String nombre = fila.getString(0);
        txtV1.setText(nombre);

        String mostrar = "";
        Cursor filaTag = dato.rawQuery("select tag from intereses where cedula=" + Integer.valueOf(seleccion), null);
        int validar = 1;
        filaTag.moveToFirst();
        while (validar <= filaTag.getCount()) {
            String tag = filaTag.getString(0);
            if(mostrar == ""){
                mostrar += tag;
            }else{
                mostrar += "\n" + tag;
            }
            validar++;
            filaTag.moveToNext();
        }
        etxt_multi.setText(mostrar);
        dato.close();
    }

    public void agregar(View view){
        //Fecha de inicio: 08/11/2020
        //Fecha de última modificación:09/11/2020
        //Este metodo extrea los datos del usuario seleccionado, de esta forma esto se agrega al arraylist de opunteros de amigos, y se guardan todos sus datos en la base de datos.
        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase dato = admin.getWritableDatabase();

        String amigoTag = spinner2.getSelectedItem().toString();
        String amigoCedula = spinner1.getSelectedItem().toString();

        Cursor filaUsuario = dato.rawQuery("select * from usuarios", null);
        int validar = 1;
        filaUsuario.moveToFirst();
        while (validar <= filaUsuario.getCount()) {
            int cedula = filaUsuario.getInt(0);
            String nombre = filaUsuario.getString(1);
            String direccion = filaUsuario.getString(2);
            int telefono = filaUsuario.getInt(3);
            met.insertarOrdenadoUsuario(cedula, nombre, direccion, telefono);
            validar++;
            filaUsuario.moveToNext();
        }

        Cursor filaTag = dato.rawQuery("select * from tag", null);
        validar = 1;
        filaTag.moveToFirst();
        while (validar <= filaTag.getCount()) {
            String nombre = filaTag.getString(0);
            String descripcion = filaTag.getString(1);
            met.insertarTag(nombre, descripcion);
            validar++;
            filaTag.moveToNext();
        }

        Cursor filaInteres = dato.rawQuery("select * from intereses", null);
        validar = 1;
        filaInteres.moveToFirst();
        while (validar <= filaInteres.getCount()) {
            int cedula = filaInteres.getInt(0);
            int id = filaInteres.getInt(1);
            String tag = filaInteres.getString(2);
            met.insertarInteresUsuario(cedula, tag);
            validar++;
            filaInteres.moveToNext();
        }

        String fecha = etF.getText().toString();
        //Log.i("Prueba fecha", fecha);
        met.insertarAmigo(Integer.valueOf(cedulaUsuario), fecha, Integer.valueOf(amigoCedula), amigoTag);
        Usuario usuario = met.inicioUsuario;
        ContentValues registro = new ContentValues();
        if (usuario != null) {
            if (usuario.sigAmigo != null) {
                Amigo aux = usuario.sigAmigo;
                while (aux != null) {
                    for (int i = 0; i < aux.sigUsuario.size(); i++) {
                        registro.put("cedula", usuario.cedula);
                        registro.put("fecha", aux.fecha);
                        String cedulaAmigo = String.valueOf(aux.sigUsuario.get(i).cedula);
                        registro.put("cedulaAmigo", Integer.valueOf(cedulaAmigo));
                        registro.put("tag", aux.sigInteres.sigTag.Nombre);
                        dato.insert("amigos", null, registro);
                    }
                    aux = aux.sig;
                }
            }
            usuario = usuario.sig;
        }
        while (usuario != met.inicioUsuario) {
            if (usuario.sigAmigo != null) {
                Amigo aux = usuario.sigAmigo;
                while (aux != null) {
                    for (int i = 0; i < aux.sigUsuario.size(); i++) {
                        registro.put("cedula", usuario.cedula);
                        registro.put("fecha", aux.fecha);
                        String cedulaAmigo = String.valueOf(aux.sigUsuario.get(i).cedula);
                        registro.put("cedulaAmigo", Integer.valueOf(cedulaAmigo));
                        registro.put("tag", aux.sigInteres.sigTag.Nombre);
                        dato.insert("amigos", null, registro);
                    }
                    aux = aux.sig;
                }
            }
            usuario = usuario.sig;
        }
        Toast.makeText(this, "Agregando...", Toast.LENGTH_LONG).show();
        dato.close();
        Intent siguiente = new Intent(this, ActivitySpinner.class);
        startActivity(siguiente);
    }

    public void volverMenu(View view){
        //Fecha de inicio: 08/11/2020
        //Fecha de última modificación:09/11/2020
        //Este metodo permite al usuario volver a la ventana del menu principal.
        Intent siguiente = new Intent(this, ActivitySpinner.class);
        startActivity(siguiente);
    }
}