package com.example.proyectoestructuras;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class ActivitySpinner extends AppCompatActivity {

    Spinner spinner1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Fecha de inicio: 7/11/2020
        //Fecha de última modificación: 7/11/2020
        //Este metodo inicializa el Spinner que le permite al usuario, desplazarse a una activity en específico.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);
        this.setTitle(R.string.Melinilla);
        spinner1 = (Spinner) findViewById(R.id.spinner2);

        String[] opciones = {"Datos Personales", "Amigos", "Intereses"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, opciones);
        spinner1.setAdapter(adapter);
    }

    public void cambiarActivity(View view) {
        //Fecha de inicio: 7/11/2020
        //Fecha de última modificación: 7/11/2020
        //Este metodo le permite al usuario, desplazarse a diferentes activities, por medio de un Spinner.
        String seleccion = spinner1.getSelectedItem().toString();
        if (seleccion.equals("Intereses")) {
            Intent siguiente = new Intent(this, AgregarIntereses.class);
            startActivity(siguiente);
        } else if (seleccion.equals("Amigos")) {
            Intent siguiente = new Intent(this, ActivityAmigos.class);
            startActivity(siguiente);
        } else if (seleccion.equals("Datos Personales")) {
            Intent siguiente = new Intent(this, ActivityDatosPersonales.class);
            startActivity(siguiente);
        }
    }
}