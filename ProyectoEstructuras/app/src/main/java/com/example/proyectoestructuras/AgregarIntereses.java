package com.example.proyectoestructuras;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class AgregarIntereses extends AppCompatActivity {
    Metodos met = new Metodos();
    private Spinner spinner;
    String cedulaUsuario = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Fecha de inicio: 07/11/2020
        //Fecha de ultima modificacion: 07/11/2020
        //método que inicia el frame junto con sus widgets, ademas extrae del share preferences el dato de cedula del usuario actual, por medio de esta cedula extrae de la base de datos los intereses que el usuario
        //anteriormente registro en la sublista de intereses, estos se guardan en un ArrayList. Se crea un nuevo arraylist con todos los tags y se recorre el primer arraylis con los intereses del usuario, de esta forma
        //los intereses que ya el usuario tenga agregados son eliminados del arraylist con los tags, por ultimo los datos que quedaron dentro de este arraylist con los tags, son mostrados en un spiner, para que el
        //usuario los pueda modificar.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_intereses);
        this.setTitle(R.string.title_activity_Intereses);
        spinner = (Spinner) findViewById(R.id.spinner);

        SharedPreferences preferencias = getSharedPreferences("agenda", Context.MODE_PRIVATE);
        String datos = preferencias.getString("cedula", "");

        cedulaUsuario = datos;

        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase dato = admin.getWritableDatabase();

        ArrayList<String> tagsUsuario = new ArrayList();
        Cursor filaInteres = dato.rawQuery("select tag from intereses where cedula=" + Integer.valueOf(datos), null);
        int validar = 1;
        filaInteres.moveToFirst();
        while (validar <= filaInteres.getCount()) {
            String nombre = filaInteres.getString(0);
            tagsUsuario.add(nombre);
            validar++;
            filaInteres.moveToNext();
        }

        ArrayList <String>opciones = new ArrayList();
        opciones.add("Gaming"); opciones.add("Lectura"); opciones.add("Peliculas"); opciones.add("Series"); opciones.add("Programacion"); opciones.add("Ciencias"); opciones.add("Deportes"); opciones.add("Gastronomia");
        opciones.add("Musica"); opciones.add("Matematicas"); opciones.add("Historia"); opciones.add("Vehiculos"); opciones.add("Fotografia"); opciones.add("Pintura"); opciones.add("Noticias");
        for (int i = 0; i < tagsUsuario.size(); i++){
            opciones.remove(tagsUsuario.get(i));
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, opciones);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        dato.close();
    }

    public void agregar(View view){
        //Fecha de inicio: 07/11/2020
        //Fecha de ultima modificacion: 08/11/2020
        //método valida el tag seleccionado por el usuario y lo ijngresa a la base de datos, y a la sublista intereses
        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase dato = admin.getWritableDatabase();

        Cursor fila = dato.rawQuery("select nombre, direccion, telefono from usuarios where cedula=" + Integer.valueOf(cedulaUsuario), null);

        if (fila.moveToFirst()) {
            String nombre = fila.getString(0);
            String direccion = fila.getString(1);
            int telefono = fila.getInt(2);
            met.insertarOrdenadoUsuario(Integer.valueOf(cedulaUsuario), nombre, direccion, telefono);
        }

        Cursor filaTag = dato.rawQuery("select * from tag", null);
        int validar = 1;
        filaTag.moveToFirst();
        while (validar <= filaTag.getCount()) {
            String nombre = filaTag.getString(0);
            String descripcion = filaTag.getString(1);
            met.insertarTag(nombre, descripcion);
            validar++;
            filaTag.moveToNext();
        }

        Cursor filaInteres = dato.rawQuery("select tag from intereses where cedula=" + Integer.valueOf(cedulaUsuario), null);
        int validar2 = 1;
        filaInteres.moveToFirst();
        while (validar2 <= filaInteres.getCount()) {
            String tag = filaInteres.getString(0);
            met.insertarInteresUsuario(Integer.valueOf(cedulaUsuario), tag);
            validar2++;
            filaInteres.moveToNext();
        }

        String seleccion = spinner.getSelectedItem().toString();
        met.insertarInteresUsuario(Integer.valueOf(cedulaUsuario), seleccion);

        Usuario usuario = met.buscarCedula(Integer.valueOf(cedulaUsuario));
        SublistaIntereses aux = usuario.sigInteres;
        ContentValues registro = new ContentValues();
        while (aux.sig != null) {
            aux = aux.sig;
        }
        registro.put("cedula", Integer.valueOf(cedulaUsuario));
        registro.put("id", aux.id);
        registro.put("tag", aux.sigTag.Nombre);
        dato.insert("intereses", null, registro);
        Toast.makeText(this, "Agregando...", Toast.LENGTH_LONG).show();
        dato.close();
        Intent siguiente = new Intent(this, ActivitySpinner.class);
        startActivity(siguiente);
    }

    public void volverMenu(View view){
        //Fecha de inicio: 07/11/2020
        //Fecha de ultima modificacion: 07/11/2020
        //método que devuelve al usuario al menu principal
        Intent siguiente = new Intent(this, ActivitySpinner.class);
        startActivity(siguiente);
    }

}