package com.example.proyectoestructuras;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;



public class Intereses extends AppCompatActivity {
    Metodos met = new Metodos();
    private CheckBox check, check2, check3, check4, check5, check6, check7, check8, check9;

    String datos = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Fecha de inicio: 6/11/2020
        //Fecha de última modificación: 7/11/2020
        //Este metodo inicializa los CheckBox que le permitirán al usuario, elegir sus diferentes intereses.
        this.setTitle(R.string.title_activity_Intereses);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intereses);

        check = (CheckBox) findViewById(R.id.checkBox);
        check2 = (CheckBox) findViewById(R.id.checkBox2);
        check3 = (CheckBox) findViewById(R.id.checkBox3);
        check4 = (CheckBox) findViewById(R.id.checkBox4);
        check5 = (CheckBox) findViewById(R.id.checkBox5);
        check6 = (CheckBox) findViewById(R.id.checkBox6);
        check7 = (CheckBox) findViewById(R.id.checkBox7);
        check8 = (CheckBox) findViewById(R.id.checkBox8);
        check9 = (CheckBox) findViewById(R.id.checkBox9);
        datos = getIntent().getStringExtra("cedulaCopia");
    }


    public void ingresar(View view) {
        //Método que cumple la función de asignarle al usuario, los intereses que fueron seleccionados en los CheckBox, además los carga a las listas y seguidamente a la base de datos.
        //También, si el usuario no ha seleccionado ningún interés, a este no se le permite seguir con su proceso de registro.
        SharedPreferences preferencias = getSharedPreferences("agenda", Context.MODE_PRIVATE);
        SharedPreferences.Editor obj_editor = preferencias.edit();
        obj_editor.putString("cedula", String.valueOf(datos));
        obj_editor.commit();

        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase dato = admin.getWritableDatabase();

        Cursor fila = dato.rawQuery("select nombre, direccion, telefono from usuarios where cedula=" + datos, null);

        if (fila.moveToFirst()) {
            String nombre = fila.getString(0);
            String direccion = fila.getString(1);
            int telefono = fila.getInt(2);
            met.insertarOrdenadoUsuario(Integer.valueOf(datos), nombre, direccion, telefono);
        }

        Cursor filaTag = dato.rawQuery("select * from tag", null);
        int validar = 1;
        filaTag.moveToFirst();
        while (validar <= filaTag.getCount()) {
            String nombre = filaTag.getString(0);
            String descripcion = filaTag.getString(1);
            met.insertarTag(nombre, descripcion);
            validar++;

            filaTag.moveToNext();
        }

        int cedulaTag = Integer.valueOf(datos);

        if (check.isChecked() == true) {
            met.insertarInteresUsuario(cedulaTag, "Musica");

        }
        if (check2.isChecked() == true) {
            met.insertarInteresUsuario(cedulaTag, "Programacion");

        }
        if (check3.isChecked() == true) {
            met.insertarInteresUsuario(cedulaTag, "Gaming");

        }
        if (check4.isChecked() == true) {
            met.insertarInteresUsuario(cedulaTag, "Series");

        }
        if (check5.isChecked() == true) {
            met.insertarInteresUsuario(cedulaTag, "Peliculas");

        }
        if (check6.isChecked() == true) {
            met.insertarInteresUsuario(cedulaTag, "Lectura");

        }
        if (check7.isChecked() == true) {
            met.insertarInteresUsuario(cedulaTag, "Gastronomia");

        }
        if (check8.isChecked() == true) {
            met.insertarInteresUsuario(cedulaTag, "Deportes");

        }
        if (check9.isChecked() == true) {
            met.insertarInteresUsuario(cedulaTag, "Ciencias");
        }

        Usuario usuario = met.buscarCedula(Integer.valueOf(datos));
        SublistaIntereses aux = usuario.sigInteres;
        ContentValues registro = new ContentValues();
        while (aux != null) {
            registro.put("cedula", Integer.valueOf(datos));
            registro.put("id", aux.id);
            registro.put("tag", aux.sigTag.Nombre);
            dato.insert("intereses", null, registro);
            aux = aux.sig;
        }
        Cursor fila1 = dato.rawQuery("select *  from intereses where cedula=" + Integer.valueOf(datos), null);
        if (fila1.getCount() != 0) {
            Toast.makeText(this, "Registrando...", Toast.LENGTH_LONG).show();
            Intent siguiente = new Intent(this, ActivitySpinner.class);
            startActivity(siguiente);
        }else{
            Toast.makeText(this,"Debe seleccionar al menos un interes",Toast.LENGTH_LONG).show();
        }
        dato.close();

    }


}


