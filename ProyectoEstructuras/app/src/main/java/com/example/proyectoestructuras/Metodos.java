package com.example.proyectoestructuras;

import android.util.Log;

import java.util.ArrayList;
import java.util.Date;

public class Metodos {

    Usuario inicioUsuario, ultimoUsuario;
    Tag inicioTag, ultimoTag;

    //-----------------------Usuario---------------------------
    public boolean insertarOrdenadoUsuario(int cedula, String nombre, String direccion, int telefono){
        //Fecha de inicio: 04/11/2020
        //Fecha de ultima modificacion: 05/11/2020
        //método que inserta a los usuarios ordenadamente de menor a mayor segun su numero de cedula.
        Usuario nuevo = new Usuario(cedula, nombre, direccion, telefono);
        if (inicioUsuario == null) {
            inicioUsuario = ultimoUsuario = nuevo;
            inicioUsuario.sig = inicioUsuario;
            inicioUsuario.ant = inicioUsuario;
            return true;
        }
        if (buscarCedula(cedula) != null) {
            return false;
        }
        Usuario aux = inicioUsuario;
        do {
            if (aux.cedula > cedula) {
                if(aux == inicioUsuario){
                    nuevo.sig = inicioUsuario;
                    inicioUsuario.ant = nuevo;
                    nuevo.ant = ultimoUsuario;
                    ultimoUsuario.sig = nuevo;
                    inicioUsuario = nuevo;
                    return true;
                }
                else{
                    aux.ant.sig = nuevo;
                    nuevo.ant = aux.ant;
                    aux.ant = nuevo;
                    nuevo.sig = aux;
                    return true;
                }
            }
            aux = aux.sig;
        } while (aux != inicioUsuario);
        ultimoUsuario.sig = nuevo;
        ultimoUsuario.sig.ant = ultimoUsuario;
        nuevo.sig = inicioUsuario;
        inicioUsuario.ant = nuevo;
        ultimoUsuario = nuevo;
        return true;
    }

    public Usuario buscarCedula(int cedula) {
        //Fecha de inicio: 04/11/2020
        //Fecha de ultima modificacion: 05/11/2020
        //método que recorre la lista para buscar una cedula igual a la recibida como parametro.
        if (inicioUsuario.cedula == cedula) {
            return inicioUsuario;
        }
        Usuario aux = inicioUsuario.sig;
        while (aux != inicioUsuario) {
            if (aux.cedula == cedula) {
                return aux;
            }
            aux = aux.sig;
        }
        return null;
    }


    //-----------------------Amigo---------------------------
    public boolean insertarAmigo(int cedula, String fecha, int cedulaAmigo, String tag) {
        //Fecha de inicio: 04/11/2020
        //Fecha de ultima modificacion: 09/11/2020
        //método que inserta en la lista a los amigos segun la cedula del usuario fecha cedula del amigo y tag en comun que lo recibe como parametro.
        Usuario usuario = buscarCedula(cedula);
        Usuario amigo = buscarCedula(cedulaAmigo);

        Amigo nuevo = new Amigo(fecha);
        if (usuario.sigAmigo == null) {
            usuario.sigAmigo = nuevo;
            nuevo.sigUsuario.add(amigo);
            insertarInteresAmigo(nuevo, tag);
            return true;
        }
        if(buscarInteresAmigo(usuario, tag) != null){
            Amigo aux = buscarInteresAmigo(usuario, tag);
            aux.sigUsuario.add(amigo);
            return true;
        }
        Amigo aux = usuario.sigAmigo;
        while (aux.sig != null){
            aux = aux.sig;
        }
        aux.sig = nuevo;
        nuevo.ant = aux;
        nuevo.sigUsuario.add(amigo);
        insertarInteresAmigo(nuevo, tag);
        return true;
    }

    public Amigo buscarInteresAmigo(Usuario usuario, String tag) {
        //Fecha de inicio: 04/11/2020
        //Fecha de ultima modificacion: 05/11/2020
        //método que busca los intereses del amigo, recibiendo el puntero, y el tag como puntero.
        Amigo aux = usuario.sigAmigo;
        while (aux != null) {
            if (aux.sigInteres.sigTag.Nombre.equals(tag)) {
                return aux;
            }
            aux = aux.sig;
        }
        return null;
    }

    //-----------------------Tag---------------------------
    public boolean insertarTag(String nombre, String descripcion) {
        //Fecha de inicio: 04/11/2020
        //Fecha de ultima modificacion: 05/11/2020
        //método que busca los intereses del amigo, recibiendo el puntero, y el tag como puntero.
        Tag nuevo = new Tag(nombre, descripcion);
        if (inicioTag == null) {
            inicioTag = ultimoTag = nuevo;
            inicioTag.sig = inicioTag;
            return true;
        }
        nuevo.sig = inicioTag;
        ultimoTag.sig = nuevo;
        inicioTag = nuevo;
        return true;
    }

    public Tag buscarTag(String nombre) {
        //Fecha de inicio: 04/11/2020
        //Fecha de ultima modificacion: 05/11/2020
        //método que busca el nodo del tag con respecto al nombre del tag que recibe como parametro.
        if (inicioTag.Nombre.equals(nombre)) {
            return inicioTag;
        }
        Tag aux = inicioTag.sig;
        while (aux != inicioTag) {
            if (aux.Nombre.equals(nombre)) {
                return aux;
            }
            aux = aux.sig;
        }
        return null;
    }

    //-----------------------Interes---------------------------
    public int insertarInteresUsuario(int cedula, String nombre) {
        //Fecha de inicio: 04/11/2020
        //Fecha de ultima modificacion: 07/11/2020
        //método que inserta los intereses del usuario con respecto a la cedula del usuario y el nombre del interes.
        Tag tag = buscarTag(nombre);
        if (buscarCedula(cedula) != null) {
            Usuario usuario = buscarCedula(cedula);
            if (tag != null) {
                if (usuario.sigInteres == null) {
                    SublistaIntereses nuevo = new SublistaIntereses(1);
                    nuevo.sigTag = tag;
                    usuario.sigInteres = nuevo;
                    return 1;
                }
                else {
                    SublistaIntereses aux = usuario.sigInteres;
                    while (aux.sig != null){
                        aux = aux.sig;
                    }
                    int id = aux.id;
                    SublistaIntereses nuevo = new SublistaIntereses(id+1);
                    nuevo.sigTag = tag;
                    aux.sig = nuevo;
                    return 1;
                }
            }
            else {
                return 2;
            }
        }
        return 0;
    }

    public boolean insertarInteresAmigo(Amigo amigo, String nombre) {
        //Fecha de inicio: 04/11/2020
        //Fecha de ultima modificacion: 05/11/2020
        //método que inserta intereses del amigo, recibiendo el puntero del amigo, y el nombre del interes .
        Tag tag = buscarTag(nombre);
        if (tag != null) {
            SublistaIntereses nuevo = new SublistaIntereses(1);
            nuevo.sigTag = tag;
            amigo.sigInteres = nuevo;
            return true;
        }
        return false;
    }

    public ArrayList mostrarAmigos(int cedula){
        //Fecha de inicio: 08/11/2020
        //Fecha de ultima modificacion: 09/11/2020
        //método recorre el arraylist de los amigos con respecto a la cedula del usuario que se manda como parametro.
        Usuario usuario = buscarCedula(cedula);
        ArrayList cedulas = new ArrayList();
        Amigo aux = usuario.sigAmigo;
        while(aux != null){
            for(int i = 0; i < aux.sigUsuario.size(); i++){
                cedulas.add(String.valueOf(aux.sigUsuario.get(i).cedula));
            }
            aux = aux.sig;
        }
        return cedulas;
    }

    public boolean eliminarAmigo(int cedula, int cedulaA) {
        //Fecha de inicio: 08/11/2020
        //Fecha de ultima modificacion: 09/11/2020
        //método que busca el amigo por medio de su cedula relacionado con la cedula del usuario, de esta forma lo remueve del array de punteros.
        Usuario usuario = buscarCedula(cedula);
        Amigo aux = usuario.sigAmigo;
        while (aux != null) {
            for (int i = 0; i < aux.sigUsuario.size(); i++) {
                if (Integer.valueOf(aux.sigUsuario.get(i).cedula) == cedulaA) {
                    aux.sigUsuario.remove(aux.sigUsuario.get(i));
                    if (aux.sigUsuario.size() == 0) {
                        if (aux == usuario.sigAmigo) {
                            aux.ant = null;
                            usuario.sigAmigo = aux.sig;
                            return true;
                        }
                        Amigo temp = usuario.sigAmigo;
                        while (temp.sig != aux) {
                            temp = temp.sig;
                        }
                        temp.sig = aux.sig;
                        if (aux.sig != null) {
                            aux.sig.ant = temp;
                        }
                        return true;
                    }
                }

            }
            aux = aux.sig;
        }
        return false;
    }
}