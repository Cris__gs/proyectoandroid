package com.example.proyectoestructuras;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Locale;



public class ModificarUbicacion extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerDragListener, GoogleMap.OnMarkerClickListener {
    Metodos met = new Metodos();
    private GoogleMap mMap;
    private Marker markerPrueba;
    String neWtitle = null;
    String latitud = "";
    String longitud = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Fecha de inicio: 06/11/2020
        //Fecha de ultima modificacion: 08/11/2020
        //método que inicializa la interfaz del mapa
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_ubicacion);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map1);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //Fecha de inicio: 07/11/2020
        //Fecha de ultima modificacion: 08/11/2020
        //método buscar en la base de datos la ultima ubicacion seleccionada por el usuario y la carga en el mapa

        mMap = googleMap;

        SharedPreferences preferencias = getSharedPreferences("agenda", Context.MODE_PRIVATE);
        String datos = preferencias.getString("cedula", "");
        //Log.i("Prueba",datos);

        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase dato = admin.getWritableDatabase();

        Cursor fila = dato.rawQuery("select direccion from usuarios where cedula=" + Integer.parseInt(datos), null);
        fila.moveToFirst();
        String direccion = fila.getString(0);


        for (int i = 0; i < direccion.length(); i++){
            if (direccion.charAt(i) == ','){
                for (int a = 1; a < i; a++){
                    latitud = latitud + direccion.charAt(a);
                }
                for (int b = i+1; b < direccion.length()-1; b++){
                    longitud = longitud + direccion.charAt(b);
                }
            }
        }
        //Log.i("Latitud", latitud);
        //Log.i("Longitud", longitud);

        LatLng prueba = new LatLng(Double.parseDouble(latitud), Double.parseDouble(longitud));
        //LatLng prueba = new LatLng(10.3381, -84.0302);
        markerPrueba = googleMap.addMarker(new MarkerOptions().position(prueba).draggable(true).title("Ubicacion"));

        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnMarkerDragListener(this);
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        //Fecha de inicio: 07/11/2020
        //Fecha de ultima modificacion: 07/11/2020
        //método que extrae la latitud y longitud de el cursor que se mueve por el mapa
        if (marker.equals(markerPrueba)){
            String lat, lng;
            lat = Double.toString(marker.getPosition().latitude);
            lng = Double.toString(marker.getPosition().longitude);
            Toast.makeText(this, lat + lng + " Direccion", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    //Drag listener
    @Override
    public void onMarkerDragStart(Marker marker) {
        //Fecha de inicio: 07/11/2020
        //Fecha de ultima modificacion: 07/11/2020
        //método que muestra un mensaje al usuario cuando empezo a mover su ubicacion por el mapa
        if (marker.equals(markerPrueba)){
            Toast.makeText(this, "Inicio", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        //Fecha de inicio: 07/11/2020
        //Fecha de ultima modificacion: 07/11/2020
        //método que cambia el titulo del frame mientras se mueve por la interfaz este muestra las coordenadas en el mismo
        if (marker.equals(markerPrueba)){
            neWtitle = String.format(Locale.getDefault(),getString(R.string.Marker_detail_lating),marker.getPosition().latitude,marker.getPosition().longitude);
            setTitle(neWtitle);
        }
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        //Fecha de inicio: 07/11/2020
        //Fecha de ultima modificacion: 07/11/2020
        //método que muestra un mensaje al usuario cuando finalizo de moverse por el mapa, , cuando se deja de mover
        //este vuelve el titulo a la normalidad son su nombre inicial
        if (marker.equals(markerPrueba)){
            Toast.makeText(this, "Finalizo", Toast.LENGTH_SHORT).show();
            setTitle(R.string.title_activity_modificar_ubicacion);
        }
    }

    public void guardar (View view){
        //Fecha de inicio: 07/11/2020
        //Fecha de ultima modificacion: 07/11/2020
        //método que envia el dato de las coordenadas hacia la clase de ActivityDatosPersonales donde este se utilizara, ademas de retrocede a este mismo frame para
        //seguir modificando los datos si los desea
        Intent i = new Intent(this, ActivityDatosPersonales.class);
        i.putExtra("dato1", neWtitle);
        //Log.i("Prueba--------------",neWtitle);
        startActivity(i);

        //Intent siguiente = new Intent(this, MainActivity.class);
        //startActivity(siguiente);

    }
}