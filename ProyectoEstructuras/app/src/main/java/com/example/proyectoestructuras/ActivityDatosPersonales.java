package com.example.proyectoestructuras;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityDatosPersonales extends AppCompatActivity {

    EditText EditTxt1, EditTxt2;
    TextView Textv1, Textv2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Fecha de incio: 07/11/2020
        //Fecha de ultima modificación: 08/11/2020
        //Este metodo inicializa los diferentes componentes de la interfaz. Además busca en la base de datos, los datos personales del usuario, para luego...
        //mostrarlos en los TextView o en los EditTexts, menos la ubicación, esta se muestra en la misma interfaz donde se encuentra el mapa.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_personales);
        this.setTitle(R.string.TituloDatos);

        EditTxt1 = (EditText)findViewById(R.id.EditTxt_nombre);
        EditTxt2 = (EditText)findViewById(R.id.EditText_telefono);
        Textv1 = (TextView)findViewById(R.id.TxtV_cedula);
        Textv2 = (TextView)findViewById(R.id.TxtV_Ubicacion);

        Textv2.setText(getIntent().getStringExtra("dato1"));

        SharedPreferences preferencias = getSharedPreferences("agenda", Context.MODE_PRIVATE);
        String datos = preferencias.getString("cedula", "");
        Log.i("Prueba",datos);

        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase dato1 = admin.getWritableDatabase();

        Cursor fila = dato1.rawQuery("select nombre, direccion, telefono from usuarios where cedula=" + datos, null);
        fila.moveToFirst();
        String nombre = fila.getString(0);
        String direccion = fila.getString(1);
        int telefono = fila.getInt(2);

        Textv1.setText(datos);
        EditTxt1.setText(String.valueOf(nombre));
        //Textv2.setText(String.valueOf(direccion));
        EditTxt2.setText(String.valueOf(telefono));

    }
    public void ubicacion (View view){
        //Fecha de incio: 07/11/2020
        //Fecha de ultima modificación: 08/11/2020
        //Metodo que le permite al usuario, desplazarse a la activity en la cual puede registrar su ubicación.
        Intent siguiente = new Intent(this, ModificarUbicacion.class);
        startActivity(siguiente);
    }

    public void irMenu(View view){
        //Fecha de incio: 07/11/2020
        //Fecha de ultima modificación: 08/11/2020
        //Metodo que le permite al usuario movilizarse al menú principal.
        Intent siguiente = new Intent(this, ActivitySpinner.class);
        startActivity(siguiente);
    }

    public void modificar(View view){

        //Este metodo le permite al usuario, modicar los datos personales que ya han sido registrados, menos la cédula.
        //Los datos que se modificaron, se actualizan en las listas y tambien en la base de datos.
        //Una vez ya modificados, los datos se muestran en los TextViews y EditTexts.
        BaseDeDatos admin = new BaseDeDatos(this, "administracion", null, 1);
        SQLiteDatabase dato1 = admin.getWritableDatabase();

        SharedPreferences preferencias = getSharedPreferences("agenda", Context.MODE_PRIVATE);
        String datos = preferencias.getString("cedula", "");
        Log.i("Prueba",datos);

        String nombre = EditTxt1.getText().toString();
        String cedula = datos;
        String telefono = EditTxt2.getText().toString();
        String direccion = Textv2.getText().toString();

        if(!nombre.isEmpty() && !telefono.isEmpty() && !direccion.isEmpty()){

            ContentValues registro = new ContentValues();
            registro.put("cedula", Integer.parseInt(cedula));
            registro.put("nombre", nombre);
            registro.put("direccion", direccion);
            registro.put("telefono", Integer.parseInt(telefono));
            //Log.i("datos------------", datos);


            int cantidad = dato1.update("usuarios", registro, "cedula=" + Integer.parseInt(datos), null);

            if(cantidad == 1){
                Textv1.setText("");
                Textv2.setText("");
                EditTxt1.setText("");
                EditTxt2.setText("");
                Cursor fila = dato1.rawQuery("select nombre, direccion, telefono from usuarios where cedula=" + datos, null);
                fila.moveToFirst();
                String nombre1 = fila.getString(0);
                int telefono1 = fila.getInt(2);

                Textv1.setText(datos);
                EditTxt1.setText(String.valueOf(nombre1));
                EditTxt2.setText(String.valueOf(telefono1));
                Toast.makeText(this, "Artículo modificado correctamente", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "El artículo no existe", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(this, "Debes llenar todos los campos", Toast.LENGTH_SHORT).show();
        }
        dato1.close();
    }
}